
import {fetchMSFT} from '../services/api'
import { tsvParse, csvParse } from  "d3-dsv";
import { timeParse } from 'd3-time-format';

export default {
  namespace: 'stockCandle',
  state: {
  },

  effects: {

    *queryMonitorInfo({ _ }, { call, put }) {
      console.info('* query monitor ');
      const response = yield call(fetchMSFT);
      yield put({
        type: 'queryMonitorInfoRet',
        payload: response,
      });

    },
  },

  reducers: {
    queryMonitorInfoRet(state, {payload}) {
      // console.log("payload==>", payload)

      function parseData(parse) {
        return function(d) {
          d.date = parse(d.date);
          d.open = +d.open;
          d.high = +d.high;
          d.low = +d.low;
          d.close = +d.close;
          d.volume = +d.volume;

          return d;
        };
      }

      const parseDate = timeParse("%Y-%m-%d");

      const stockData = tsvParse(payload, parseData(parseDate))
      return {
        ...state,
        data: stockData,
      };
    },

  },

};
