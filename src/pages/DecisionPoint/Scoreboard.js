import React from 'react';
import { Card, Row, Col } from 'antd';

export default class Scoreboard extends React.Component {

  render() {
    console.log("Scoreboard=", this.props.location.query)
    return (
      <Row>
        <Col span={12}>
          <Card
            title="Fear & Greed Index"
            extra={<a href="https://money.cnn.com/data/fear-and-greed/" target="_blank">More</a>}
            style={{ height: 400 }}
          >
            <img src="https://money.cnn.com/.element/img/5.0/data/feargreed/1.png" />
          </Card>
        </Col>
        <Col span={12}>
          <Card title="Fear & Greed Over Time" style={{ height: 400 }}>
            <img src="https://markets.money.cnn.com/Marketsdata/Api/Chart/FearGreedHistoricalImage?chartType=AvgPtileModel" />
          </Card>
        </Col>
      </Row>
    );
  }
}
