import CandleStickChartWithMACDIndicator from './KLine'

import router from 'umi/router';
import React from 'react';
import {Row, Col, Card, Form, Input, Select, Button, Icon} from 'antd'

@Form.create()
export default class StockCandle extends  React.Component{

  state = {
    currency: 'RMB'
  }

  toDes = () => {
    router.push("/form?a=1&b=2")
  }


  render(){
    console.log("StockCandle= ", this.props)
    const { getFieldDecorator } = this.props.form;
    const { Option } = Select;
    return <div>
      <Card>
        <Form layout="inline" onSubmit={this.handleSubmit}>
          <Form.Item label="Symbol">
            {getFieldDecorator('userName', {
              rules: [{ required: true, message: 'Please input your username!' }],
            })(
              <Input placeholder="Username" />
            )}
          </Form.Item>
          <Form.Item
            label="Period"
            hasFeedback
          >
            <Select defaultValue="1">
              <Option value="1">Day</Option>
              <Option value="2">Week</Option>
              <Option value="3">Month</Option>
            </Select>
          </Form.Item>
        </Form>
      </Card>
      <Card>
        <CandleStickChartWithMACDIndicator/>
      </Card>

      <Card
        title="Indicators"
      >
        <Form layout="inline" onSubmit={this.handleSubmit}>
          <Row>
            <Col span={4}>
              <Form.Item label="type">
                {getFieldDecorator('userName', {
                  rules: [{ required: true, message: 'Please input your username!' }],
                })(
                  <Input placeholder="Username" />
                )}
              </Form.Item>
            </Col>
            <Col span={4}>
              <Form.Item label="type">
                {getFieldDecorator('userName', {
                  rules: [{ required: true, message: 'Please input your username!' }],
                })(
                  <Input placeholder="Username" />
                )}
              </Form.Item>
            </Col>
          </Row>

          <Row>
            <Col span={4}>
              <Form.Item label="type">
                {getFieldDecorator('userName', {
                  rules: [{ required: true, message: 'Please input your username!' }],
                })(
                  <Input placeholder="Username" />
                )}
              </Form.Item>
            </Col>
            <Col span={4}>
              <Form.Item label="type">
                {getFieldDecorator('userName', {
                  rules: [{ required: true, message: 'Please input your username!' }],
                })(
                  <Input placeholder="Username" />
                )}
              </Form.Item>
            </Col>
          </Row>

          <Row>
            <Col span={4}>
              <Form.Item label="type">
                {getFieldDecorator('userName', {
                  rules: [{ required: true, message: 'Please input your username!' }],
                })(
                  <Input placeholder="Username" />
                )}
              </Form.Item>
            </Col>
            <Col span={4}>
              <Form.Item label="type">
                {getFieldDecorator('userName', {
                  rules: [{ required: true, message: 'Please input your username!' }],
                })(
                  <Input placeholder="Username" />
                )}
              </Form.Item>
            </Col>
          </Row>
        </Form>
        <Button type="primary">Update</Button>

      </Card>


    </div>
  }

}
