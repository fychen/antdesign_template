export default [
  // user
  {
    path: '/user',
    component: '../layouts/UserLayout',
    routes: [
      { path: '/user', redirect: '/user/login' },
      { path: '/user/login', component: './User/Login' },
      { path: '/user/register', component: './User/Register' },
      { path: '/user/register-result', component: './User/RegisterResult' },
    ],
  },
  // app
  {
    path: '/',
    component: '../layouts/BasicLayout',
    Routes: ['src/pages/Authorized'],
    authority: ['admin', 'user'],
    routes: [
      // dashboard
      { path: '/', redirect: '/Stock/StockCandle' },
      {
        path: '/Stock/StockCandle',
        name: 'dashboard',
        icon: 'dashboard',
        component: './Stock/StockCandle',
      },

      // DecisionPoint Scoreboard
      {
        path: '/form',
        icon: 'form',
        name: 'form',
        component: './DecisionPoint/Scoreboard',
      },


      {
        component: '404',
      },
    ],
  },
];
